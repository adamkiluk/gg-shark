﻿namespace GG_Shark.Persistence
{
    using GG_Shark.Application.Interfaces;
    using GG_Shark.Domain.Entity;
    using Microsoft.EntityFrameworkCore;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;

    public class GGSharkDbContext : DbContext, IGGSharkDbContext
    {
        public GGSharkDbContext(DbContextOptions<GGSharkDbContext> options)
            : base(options)
        {

        }
        public DbSet<User> Users {get; set;}
        public DbSet<Signboard> Signboards { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<CompanyPerks> Perks { get; set; }
        public DbSet<CompanyBenefits> Benefits { get; set; }
        public DbSet<CompanyTechnologies> Technologies { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(GGSharkDbContext).Assembly);

            modelBuilder.Entity<Signboard>().Property(p => p.SeniorityLevel)
            .HasConversion(
                v => JsonConvert.SerializeObject(v),
                v => JsonConvert.DeserializeObject<List<string>>(v));

            modelBuilder.Entity<Signboard>().Property(p => p.MustHave)
            .HasConversion(
                v => JsonConvert.SerializeObject(v),
                v => JsonConvert.DeserializeObject<List<string>>(v));

            modelBuilder.Entity<Signboard>().Property(p => p.NiceToHave)
            .HasConversion(
                v => JsonConvert.SerializeObject(v),
                v => JsonConvert.DeserializeObject<List<string>>(v));

            modelBuilder.Entity<Signboard>().Property(p => p.DailyTasks)
            .HasConversion(
                v => JsonConvert.SerializeObject(v),
                v => JsonConvert.DeserializeObject<List<string>>(v));
        }
    }
}
