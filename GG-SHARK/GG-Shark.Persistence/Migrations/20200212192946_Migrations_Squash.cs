﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GG_Shark.Persistence.Migrations
{
    public partial class Migrations_Squash : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    CompanyId = table.Column<Guid>(nullable: false),
                    CompanyInfoId = table.Column<Guid>(nullable: true),
                    CompanyJobPostsId = table.Column<Guid>(nullable: true),
                    CEOId = table.Column<Guid>(nullable: true),
                    CTOId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.CompanyId);
                });

            migrationBuilder.CreateTable(
                name: "CompanyBios",
                columns: table => new
                {
                    CompanyBioId = table.Column<Guid>(nullable: false),
                    CompanyId = table.Column<Guid>(nullable: false),
                    About = table.Column<string>(nullable: true),
                    CompanySize = table.Column<int>(nullable: false),
                    Location = table.Column<string>(nullable: true),
                    FoundedIn = table.Column<DateTime>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    Facebook = table.Column<string>(nullable: true),
                    LinkedIn = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyBios", x => x.CompanyBioId);
                });

            migrationBuilder.CreateTable(
                name: "CompanyStacks",
                columns: table => new
                {
                    CompanyStackId = table.Column<Guid>(nullable: false),
                    CompanyId = table.Column<Guid>(nullable: false),
                    TechnologyStack = table.Column<string>(nullable: true),
                    Benefits = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyStacks", x => x.CompanyStackId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    CompanyId = table.Column<Guid>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_Users_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Signboards",
                columns: table => new
                {
                    SignboardId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    CompanyId = table.Column<Guid>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    MustHave = table.Column<string>(nullable: true),
                    NiceToHave = table.Column<string>(nullable: true),
                    SalaryRange = table.Column<string>(nullable: true),
                    Categories = table.Column<string>(nullable: true),
                    Seniority = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Signboards", x => x.SignboardId);
                    table.ForeignKey(
                        name: "FK_Signboards_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Signboards_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Signboards_CompanyId",
                table: "Signboards",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Signboards_UserId",
                table: "Signboards",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_CompanyId",
                table: "Users",
                column: "CompanyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompanyBios");

            migrationBuilder.DropTable(
                name: "CompanyStacks");

            migrationBuilder.DropTable(
                name: "Signboards");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Companies");
        }
    }
}
