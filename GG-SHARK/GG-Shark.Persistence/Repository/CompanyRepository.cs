﻿namespace GG_Shark.Persistence.Repository
{
    using GG_Shark.Application.Interfaces;
    using GG_Shark.Domain.Entity;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    public class CompanyRepository : Repository<Company>, ICompanyRepository
    {
        public CompanyRepository(GGSharkDbContext context) : base(context)
        {

        }

        public async Task<Company> GetUserCompany(Guid? companyId)
        {
            return await base._context.Companies.FirstOrDefaultAsync(x => x.CompanyId.Equals(companyId));
        }
    }
}
