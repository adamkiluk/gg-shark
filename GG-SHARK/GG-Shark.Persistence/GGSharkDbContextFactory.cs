﻿namespace GG_Shark.Persistence
{
    using Microsoft.EntityFrameworkCore;
    using GG_Shark.Persistence.Infrastructure;

    public class GGSharkDbContextFactory : DesignTimeDbContextFactoryBase<GGSharkDbContext>
    {
        protected override GGSharkDbContext CreateNewInstance(DbContextOptions<GGSharkDbContext> options)
        {
            return new GGSharkDbContext(options);
        }
    }
}
