﻿namespace GG_Shark.Domain.Enums
{
    public enum SingboardSeniorityLevel
    {
        Trainee,
        Junior,
        Mid,
        Senior,
        Expert
    }
}
