﻿namespace GG_Shark.FunctionalTests.Infrastructure
{
    using GG_Shark.Domain.Entity;
    using GG_Shark.Domain.Enums;
    using GG_Shark.Persistence;
    using System;

    class ContextDataSeeding
    {
        public static void Run(ref GGSharkDbContext context)
        {
            //context.Signboards.AddRange(AddSignboardsToDatabase());

            context.SaveChanges();
        }

        //public static Signboard[] AddSignboardsToDatabase()
        //{
        //    int singboardsCount = 20;
        //    var singboards = new Signboard[singboardsCount];

        //    for (int i = 0; i < singboardsCount; i++)
        //    {
        //        singboards[i] = new Signboard
        //        {
        //            UserId = Guid.NewGuid(),
        //            CompanyId = Guid.NewGuid(),
        //            CompanyName = $"Company_nr{i}",
        //            MustHave = $".Net, C#, React, {i}",
        //            NiceToHave = $"Angular, Docker, NodeJs, {i}",
        //            SalaryRange = $"{i}00 - {i + 2}00",
        //            Categories = $"{i}",
        //            Seniority = SingboardSeniorityLevel.Expert

        //        };
        //    }

        //    return singboards;
        //}
    }
}
