﻿namespace GG_Shark.Tests.Infrastructure
{
    using GG_Shark.Application.Interfaces;
    using GG_Shark.Persistence;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Configuration;

    public class ServiceModel
    {
        public GGSharkDbContext Context { get; set; }
        public IConfiguration Configuration { get; set; }
        public IHttpContextAccessor HttpContextAccessor { get; set; }
        public INotificationService NotificationService { get; set; }
    }
}
