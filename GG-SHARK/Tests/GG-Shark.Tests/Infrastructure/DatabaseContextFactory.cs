﻿namespace GG_Shark.Tests.Infrastructure
{
    using GG_Shark.Domain.Entity;
    using GG_Shark.Domain.Enums;
    using GG_Shark.Persistence;
    using Microsoft.EntityFrameworkCore;
    using System;

    public class DatabaseContextFactory
    {
        public static GGSharkDbContext Create()
        {
            var options = new DbContextOptionsBuilder<GGSharkDbContext>()
                               .UseInMemoryDatabase(Guid.NewGuid().ToString())
                               .EnableSensitiveDataLogging(true)
                               .Options;

            var context = new GGSharkDbContext(options);

            //context.Signboards.AddRange(AddSingboardsToDatabase());

            context.SaveChanges();

            return context;
        }

        public static void Destroy(GGSharkDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Dispose();
        }

        //private static Signboard[] AddSingboardsToDatabase()
        //{
        //    int singboardsCount = 20;
        //    var singboards = new Signboard[singboardsCount];
            
        //    for(int i = 0; i < singboardsCount; i++)
        //    {
        //        singboards[i] = new Signboard
        //        {
        //            UserId = Guid.NewGuid(),
        //            CompanyId = Guid.NewGuid(),
        //            CompanyName = $"Company_nr{i}",
        //            MustHave = $".Net, C#, React, {i}",
        //            NiceToHave = $"Angular, Docker, NodeJs, {i}",
        //            SalaryRange = $"{i}00 - {i + 2}00",
        //            Categories = $"{i}",
        //            Seniority = SingboardSeniorityLevel.Expert

        //        };
        //    }

        //    return singboards;
        //}
    }
}
