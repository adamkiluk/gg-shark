﻿namespace GG_Shark.Tests.Signboard.Command
{
    using FluentValidation;
    using GG_Shark.Application.Interfaces;
    using GG_Shark.Application.Signboards.Command.Delete;
    using GG_Shark.Persistence;
    using GG_Shark.Persistence.Repository;
    using GG_Shark.Tests.Infrastructure;
    using Microsoft.EntityFrameworkCore;
    using Shouldly;
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Xunit;

    [Collection("ServicesTestCollection")]
    public class DeleteSignboardCommandTest
    {
        private readonly GGSharkDbContext _context;
        private readonly SignboardRepository _signboardRepository;
        private readonly INotificationService _notificationService;

        public DeleteSignboardCommandTest(ServiceFixture fixture)
        {
            _context = fixture.Context;
            _signboardRepository = new SignboardRepository(fixture.Context);
            _notificationService = fixture.NotificationService;
        }

        [Fact]
        public async Task Delete_Signboard_Should_Delete_Signboard_From_Database()
        {
            var signboardInDatabase = _context.Signboards.FirstAsync().Result;

            var command = new DeleteSignboardCommand
            {
                SignboardId = signboardInDatabase.SignboardId
            };

            var commandHandler = new DeleteSignboardCommand.Handler(_signboardRepository);

            await commandHandler.Handle(command, CancellationToken.None);

            var signboard = _context.Signboards.FirstOrDefaultAsync(x => x.SignboardId == command.SignboardId).Result;

            signboard.ShouldBeNull();
        }

        [Fact]
        public async Task Delete_Signboard_With_Wrong_Data_Should_Throw_Exception()
        {
            var command = new DeleteSignboardCommand
            {
                SignboardId = Guid.Empty,
            };

            var commandHandler = new DeleteSignboardCommand.Handler(_signboardRepository);

            await commandHandler.Handle(command, CancellationToken.None)
                                .ShouldThrowAsync<ValidationException>();
        }
    }
}
