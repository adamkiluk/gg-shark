﻿namespace GG_Shark.Application.Users
{
    using AutoMapper;
    using GG_Shark.Application.Users.Command.Edit;
    using GG_Shark.Application.Users.Query.GetUserData.DTO;
    using GG_Shark.Domain.Entity;

    public class UserMapProfile : Profile
    {
        public UserMapProfile()
        {
            // => GetUserData
            CreateMap<User, UserDTO>(MemberList.Destination);
            CreateMap<Company, CompanyDTO>(MemberList.Destination);

            // => EditUserData
            CreateMap<EditUserDTO, User>(MemberList.Source);
        }
    }
}
