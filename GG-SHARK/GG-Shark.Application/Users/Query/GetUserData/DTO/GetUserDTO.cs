﻿namespace GG_Shark.Application.Users.Query.GetUserData.DTO
{
    public class GetUserDTO
    {
        public UserDTO User { get; set; }
        public CompanyDTO Company{ get; set; }
    }
}