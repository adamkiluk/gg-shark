﻿namespace GG_Shark.Application.Users.Query.GetUserData
{
    using GG_Shark.Application.Users.Query.GetUserData.DTO;
    using MediatR;

    public class GetUserDataQuery : IRequest<GetUserDTO>
    {
        public string UserToken { get; set; }
    }
}
