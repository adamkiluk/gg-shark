﻿namespace GG_Shark.Application.Authentication.Model
{
    public class LoginUserModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
