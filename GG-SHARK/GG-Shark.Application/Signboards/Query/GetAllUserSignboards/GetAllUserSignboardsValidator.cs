﻿namespace GG_Shark.Application.Signboards.Query.GetAllUserSignboards
{
    using FluentValidation;
    using GG_Shark.Application.Helpers;

    public class GetAllUserSignboardsValidator : AbstractValidator<GetAllUserSignboardsQuery>
    {
        public GetAllUserSignboardsValidator()
        {
            RuleFor(x => x.UserId)
                .NotEmpty()
                .Must(GuidValidator.IsGUID);
        }
    }
}
