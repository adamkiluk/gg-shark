﻿namespace GG_Shark.Application.Signboards.Query.GetAllUserSignboards
{
    using GG_Shark.Domain.Entity;
    using System.Collections.Generic;

    public class SignboardModel
    {
        public List<Signboard> AllSignboards { get; set; }

    }
}
