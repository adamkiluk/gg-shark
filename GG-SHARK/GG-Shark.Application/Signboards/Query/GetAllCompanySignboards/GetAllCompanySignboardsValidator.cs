﻿namespace GG_Shark.Application.Signboards.Query.GetAllCompanySignboards
{
    using FluentValidation;
    using GG_Shark.Application.Helpers;

    public class GetAllCompanySignboardsValidator : AbstractValidator<GetAllCompanySignboardsQuery>
    {
        public GetAllCompanySignboardsValidator()
        {
            RuleFor(x => x.CompanyId)
                .NotEmpty()
                .Must(GuidValidator.IsGUID);
        }
    }
}
