﻿namespace GG_Shark.Application.Signboards.Query.GetAllCompanySignboards
{
    using GG_Shark.Domain.Entity;
    using System.Collections.Generic;

    public class CompanySignboardModel
    {
        public List<Signboard> AllCompanySignboards { get; set; }
    }
}
