﻿namespace GG_Shark.Application.Signboards
{
    using AutoMapper;
    using GG_Shark.Application.Signboards.Command.Create;
    using GG_Shark.Domain.Entity;

    public class SignboardMapProfile : Profile
    {
        public SignboardMapProfile()
        {
            // => CreateSignboard
            CreateMap<CreateSignboardModel, Signboard>(MemberList.Source);
        }
    }
}