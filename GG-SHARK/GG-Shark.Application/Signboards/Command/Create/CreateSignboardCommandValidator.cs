﻿namespace GG_Shark.Application.Signboards.Command.Create
{
    using FluentValidation;
    using GG_Shark.Application.Helpers;

    public class CreateSignboardCommandValidator : AbstractValidator<CreateSignboardCommand>
    {
        public CreateSignboardCommandValidator()
        {
            RuleFor(x => x.Model.DailyTasks)
                .NotEmpty();

            RuleFor(x => x.Model.MustHave)
                .NotEmpty();
            RuleFor(x => x.Model.NiceToHave)
               .NotEmpty();
        }
    }
}
