﻿namespace GG_Shark.Application.Signboards.Command.Delete
{
    using FluentValidation;
    using GG_Shark.Application.Helpers;

    public class DeleteSignboardCommandValidator : AbstractValidator<DeleteSignboardCommand>
    {
        public DeleteSignboardCommandValidator()
        {
            RuleFor(x => x.SignboardId)
                .NotEmpty()
                .Must(GuidValidator.IsGUID);
        }
    }
}
