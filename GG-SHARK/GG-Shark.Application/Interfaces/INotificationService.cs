﻿namespace GG_Shark.Application.Interfaces
{
    using System.Threading.Tasks;

    public interface INotificationService
    {
        Task SendMessageToAll(string message);
    }
}
