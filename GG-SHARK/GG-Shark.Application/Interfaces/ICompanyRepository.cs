﻿namespace GG_Shark.Application.Interfaces
{
    using GG_Shark.Domain.Entity;
    using System;
    using System.Threading.Tasks;

    public interface ICompanyRepository : IRepository<Company>
    {
        Task<Company> GetUserCompany(Guid? userId);
    }
}
