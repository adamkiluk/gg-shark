import React, { ReactNode } from 'react';
import Layout from './Layout';
import { Route, Switch } from 'react-router-dom';
import { RoutesConfig } from './config/routes.config';
import { Portal, Login, Register, Account, JobForm } from './containers';

export const routes: ReactNode = (
	<Layout>
		<Switch>
			<Route path={RoutesConfig.PostJob.path} component={JobForm} />
			<Route exact path={RoutesConfig.Portal.path} component={Portal} />
			<Route path={RoutesConfig.Login.path} component={Login} />
			<Route path={RoutesConfig.Register.path} component={Register} />
			<Route path={RoutesConfig.Account.path} component={Account} />
		</Switch>
	</Layout>
);
