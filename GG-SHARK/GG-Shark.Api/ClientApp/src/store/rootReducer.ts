import { History } from 'history';
import { IApplicationState } from './index';
import { reducer as AuthReducer } from './auth';
import { reducer as AccReducer } from './Account';
import { reducer as JobFormReducer } from './JobForm';
import { reducer as PortalReducer } from './Portal';
import { combineReducers, Reducer } from 'redux';
import { connectRouter } from 'connected-react-router';

/**
 * Takes all the individual reducers and creates a single state object by combining them.
 */
export const createRootReducer = (history: History): Reducer<IApplicationState> =>
	combineReducers<IApplicationState>({
		auth: AuthReducer,
		acc: AccReducer,
		jobForm: JobFormReducer,
		portal: PortalReducer,
		router: connectRouter(history),
	});
