import { FunctionReturnTypes, ReduxAction } from '..';
import { actionCreators } from './actions';
import { IJobFormState, ActionType } from './types';
import update from 'immutability-helper';

const initialState = Object.freeze<IJobFormState>({
	newSignboard: {
		title: '',
		about: '',
		seniorityLevel: [],
		mustHave: [],
		niceToHave: [],
		dailyTasks: [],
		salaryFrom: 0,
		salaryTo: 0,
		hiddenRange: false,
	},
	sending: false,
	send: false,
});

export const reducer = (state: IJobFormState = initialState, incomingAction: FunctionReturnTypes<typeof actionCreators>) => {
	const action = incomingAction as ReduxAction;

	switch (action.type) {
		case ActionType.SETSENIORITYLEVEL:
			return update(state, {
				newSignboard: {
					seniorityLevel: { $set: action.seniorityLevel },
				},
			});
		case ActionType.SETSKILL:
			return update(state, {
				newSignboard: {
					[action.skillType]: { $push: [action.skill] },
				},
			});
		case ActionType.UNSETSKILL:
			return update(state, {
				newSignboard: {
					[action.skillType]: { $set: action.skills },
				},
			});
		case ActionType.SENDINGSIGNBOARD:
			return update(state, {
				sending: { $set: true },
				send: { $set: false },
			});
		case ActionType.SENDSIGNBOARD:
			return update(state, {
				sending: { $set: false },
				send: { $set: true },
			});
		case ActionType.RESET_STATE:
			return initialState;
		default:
			return state;
	}
};
