export interface IActionType {
	readonly RESET_STATE: string;
	readonly SETSENIORITYLEVEL: string;
	readonly SETSKILL: string;
	readonly UNSETSKILL: string;
	readonly SENDINGSIGNBOARD: string;
	readonly SENDSIGNBOARD: string;
}

export type IJobFormState = {
	newSignboard: ISignboard;
	sending: boolean;
	send: boolean;
};

export type ISignboard = {
	title: string;
	seniorityLevel: Array<string>;
	about: string;
	hiddenRange: boolean;
	salaryFrom: Number;
	salaryTo: Number;
	mustHave: Array<string>;
	niceToHave: Array<string>;
	dailyTasks: Array<string>;
};

export enum seniorityLevel {
	Trainee,
	Junior,
	Mid,
	Senior,
	Expert,
}

const _namespace = 'JOBFORM';

export const ActionType = Object.freeze<IActionType>({
	RESET_STATE: `${_namespace}/RESET_STATE`,
	SETSENIORITYLEVEL: `${_namespace}/SETSENIORITYLEVEL`,
	SETSKILL: `${_namespace}/SETSKILL`,
	UNSETSKILL: `${_namespace}/UNSETSKILL`,
	SENDINGSIGNBOARD: `${_namespace}/SENDINGSIGNBOARD`,
	SENDSIGNBOARD: `${_namespace}/SENDSIGNBOARD`,
});
