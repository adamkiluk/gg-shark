import { ReduxAction, IAppThunkAction } from '../';
import { ActionType, ISignboard } from './types';
import { badgeColors } from '../../helpers/constantsTypes';
import _ from 'lodash';
import { SignboardApi } from '../../api';

export const actionCreators = {
	resetState: (): ReduxAction => ({
		type: ActionType.RESET_STATE,
	}),

	setSeniorityLevel: (seniorityLevel: string[]): IAppThunkAction<ReduxAction> => (dispatch) => {
		dispatch({
			seniorityLevel,
			type: ActionType.SETSENIORITYLEVEL,
		});
	},

	setSkill: (skillType: string, skill: string): IAppThunkAction<ReduxAction> => (dispatch) => {
		dispatch({
			skillType,
			skill,
			type: ActionType.SETSKILL,
		});
	},

	unsetSkill: (skillType: string, skills: Array<string>): IAppThunkAction<ReduxAction> => (dispatch) => {
		dispatch({
			skillType,
			skills,
			type: ActionType.UNSETSKILL,
		});
	},

	sendJobOffer: (signboard: ISignboard): IAppThunkAction<ReduxAction> => (dispatch) => {
		dispatch({
			type: ActionType.SENDINGSIGNBOARD,
		});
		debugger;
		SignboardApi.sendSignboard(signboard).then(() => {
			debugger;
			dispatch({
				type: ActionType.SENDSIGNBOARD,
			});
		});
	},
	// loginUserRequest: (credentials: ICredentials): IAppThunkAction<ReduxAction> => (dispatch) => {
	// 	AuthApi.loginAsync(credentials).then((authUser: IAuthUser) => {
	// 		if (authUser.status === AuthStatusEnum.SUCCESS) {
	// 			dispatch({
	// 				authUser,
	// 				type: ActionType.LOGIN_SUCCESS,
	// 			});

	// 			Cookies.set('authToken', authUser.token, { expires: new Date(authUser.validTo) });
	// 		} else {
	// 			dispatch({ type: ActionType.LOGIN_FAIL });
	// 		}
	// 	});
	// },
};
