import { FunctionReturnTypes, ReduxAction } from '..';
import { actionCreators } from './actions';
import { ActionType, IAccountState, IAccount, ICompany } from './types';
import update from 'immutability-helper';

const initialState = Object.freeze<IAccountState>({
	userAccount: {} as IAccount,
	userCompany: {} as ICompany,
	message: '',
	downloading: false,
	downloaded: false,
});

export const reducer = (state: IAccountState = initialState, incomingAction: FunctionReturnTypes<typeof actionCreators>) => {
	const action = incomingAction as ReduxAction;

	switch (action.type) {
		case ActionType.GET_USER:
			return update(state, {
				downloading: { $set: true },
			});

		case ActionType.GET_USER_FULFILED:
			return update(state, {
				userAccount: { $set: action.account.user },
				userCompany: { $set: action.account.company },
				message: { $set: 'User downloaded' },
				downloading: { $set: false },
				downloaded: { $set: true },
			});

		case ActionType.GET_USER_REJECTED:
			return update(state, {
				message: { $set: 'Unable to get user' },
				downloading: { $set: false },
				downloaded: { $set: false },
			});

		case ActionType.EDIT_USER_FULFILED:
			return update(state, {
				userAccount: { $set: action.account },
				message: { $set: 'User edited' },
			});
		case ActionType.EDIT_USER_REJECTED:
			return update(state, {
				message: { $set: 'Unable to edit user' },
			});
		default:
			return state;
	}
};
