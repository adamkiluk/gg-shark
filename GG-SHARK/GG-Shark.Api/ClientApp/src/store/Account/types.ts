export interface IActionType {
	readonly RESET_STATE: string;

	readonly GET_USER: string;
	readonly GET_USER_FULFILED: string;
	readonly GET_USER_REJECTED: string;

	readonly EDIT_USER: string;
	readonly EDIT_USER_FULFILED: string;
	readonly EDIT_USER_REJECTED: string;
}
export type IAccount = {
	email: string;
	name: string;
	phoneNumber: string;
	repository: string;
	website: string;
	facebook: string;
	linkedin: string;
	termsAndPolicy: boolean;
	newsletterSub: boolean;
};
export type ICompany = {
	name: string;
	aboutPart1: string;
	aboutPart2: string;
	foundedIn: Date;
	location: string;
	companySize: number;
	url: string;
	facebook: string;
	linkedIn: string;
	perks: Object;
	benefits: Object;
	technologies: Object;
};
export type IAccountState = {
	userAccount: IAccount;
	userCompany: ICompany;
	message: string;
	downloading: boolean;
	downloaded: boolean;
};

export type IToken = {
	token?: string;
};

export type EditUserModel = IAccount & IToken;

const _namespace = 'ACCOUNT';

export const ActionType = Object.freeze<IActionType>({
	RESET_STATE: `${_namespace}/RESET_STATE`,

	GET_USER: `${_namespace}/GET_USER`,
	GET_USER_FULFILED: `${_namespace}/GET_USER_FULFILED`,
	GET_USER_REJECTED: `${_namespace}/GET_USER_REJECTED`,

	EDIT_USER: `${_namespace}/EDIT_USER`,
	EDIT_USER_FULFILED: `${_namespace}/EDIT_USER_FULFILED`,
	EDIT_USER_REJECTED: `${_namespace}/EDIT_USER_REJECTED`,
});
