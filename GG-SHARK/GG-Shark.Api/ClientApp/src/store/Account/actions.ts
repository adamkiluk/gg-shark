import { AccApi } from '../../api';
import { ReduxAction, IAppThunkAction } from '../';
import { ActionType, IAccount, EditUserModel } from './types';
import Cookies from 'js-cookie';

export const actionCreators = {
	resetState: (): ReduxAction => ({
		type: ActionType.RESET_STATE,
	}),

	getUser: (): IAppThunkAction<ReduxAction> => (dispatch) => {
		dispatch({ type: ActionType.GET_USER });

		AccApi.getUserAsync().then((account: IAccount) => {
			if (account !== null) {
				dispatch({ account, type: ActionType.GET_USER_FULFILED });
			} else {
				dispatch({ type: ActionType.GET_USER_REJECTED });
			}
		});
	},

	editUser: (account: IAccount): IAppThunkAction<ReduxAction> => (dispatch) => {
		const model: EditUserModel = {
			...account,
			token: Cookies.get('authToken') as string,
		};

		AccApi.editUserAsync(model).then((response) => {
			if (response) {
				dispatch({ account, type: ActionType.EDIT_USER_FULFILED });
			} else {
				dispatch({ type: ActionType.EDIT_USER_REJECTED });
			}
		});
	},
};
