export interface IActionType {
	readonly RESET_STATE: string;
}

export type IPortalState = {
	sending: boolean;
	send: boolean;
};

export type ISignboard = {
	dailyTasks: Array<string>;
};

export enum seniorityLevel {
	Trainee,
	Junior,
	Mid,
	Senior,
	Expert,
}

const _namespace = 'PORTAL';

export const ActionType = Object.freeze<IActionType>({
	RESET_STATE: `${_namespace}/RESET_STATE`,
});
