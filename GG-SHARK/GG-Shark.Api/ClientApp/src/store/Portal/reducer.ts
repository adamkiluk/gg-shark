import { FunctionReturnTypes, ReduxAction } from '..';
import { actionCreators } from './actions';
import { ActionType, IPortalState } from './types';
import update from 'immutability-helper';

const initialState = Object.freeze<IPortalState>({
	sending: false,
	send: false,
});

export const reducer = (state: IPortalState = initialState, incomingAction: FunctionReturnTypes<typeof actionCreators>) => {
	const action = incomingAction as ReduxAction;

	switch (action.type) {
		case ActionType.RESET_STATE:
			return initialState;
		default:
			return state;
	}
};
