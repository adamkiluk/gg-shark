import React, { ReactNode } from 'react';
import styled from 'styled-components';

const ToastifyMsg = styled.div`
  span {
    font-size: 1.075rem;
  }

  svg {
    font-size: 1.25rem;
    margin: 0 .75rem 0 .5rem;
  }
`;

const renderToastifyMsg = (message: string): ReactNode => (
  <ToastifyMsg>
    <span>{message}</span>
  </ToastifyMsg>
);

export {
  renderToastifyMsg
};