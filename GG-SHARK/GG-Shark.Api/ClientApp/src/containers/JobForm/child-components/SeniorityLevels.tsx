import React, { Fragment, ReactNode, useEffect } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { IApplicationState } from '../../../store';
import { actionCreators, reducer } from '../../../store/JobForm';
import { StyledFormTitle } from './styledComponents';
import _ from 'lodash';
import './styles.css';

const seniorityEnum = ['Trainee', 'Junior', 'Mid', 'Senior', 'Expert'];
const SeniorityWrapper = styled.div`
	width: 120px;
	height: 60px;
	padding: 10px;
	display: flex;
	justify-content: center;
	align-items: center;
	margin-left: auto;
	margin-right: auto;
	:hover {
		background-color: #4285f4;
		color: white;
		font-weight: 500;
		cursor: pointer;
	}
`;

type SeniorityLevelsProps = ReturnType<typeof reducer> & typeof actionCreators;

const SeniorityLevels: React.FC<SeniorityLevelsProps> = ({ newSignboard, setSeniorityLevel }) => {
	useEffect(() => {
		renderSeniorityList();
	}, [newSignboard.seniorityLevel]);

	const handleSeniority = (event: React.MouseEvent<HTMLDivElement, MouseEvent>): void => {
		let selectedLevel: string = (event.target as HTMLTextAreaElement).innerText;
		let seniorityArray: Array<string> = newSignboard.seniorityLevel == undefined ? [] : newSignboard.seniorityLevel;
		if (seniorityArray.includes(selectedLevel)) {
			setSeniorityLevel(_.difference(seniorityArray, [selectedLevel]));
		} else {
			setSeniorityLevel(_.union(seniorityArray, [selectedLevel]));
		}
	};

	const renderSeniorityList = (): ReactNode => {
		return seniorityEnum.map(
			(item: string): ReactNode => {
				return (
					<SeniorityWrapper
						key={item}
						className={newSignboard.seniorityLevel?.includes(item) ? 'hooverEffect' : ''}
						onClick={handleSeniority}
					>
						{item}
					</SeniorityWrapper>
				);
			},
		);
	};

	return (
		<Fragment>
			<div className="container px-4 mx-4">
				<div className="row border-bottom border-light">{renderSeniorityList()}</div>
			</div>
		</Fragment>
	);
};
const mapStateToProps = (state: IApplicationState) => state.jobForm;

export default connect(mapStateToProps, actionCreators)(SeniorityLevels as any);
