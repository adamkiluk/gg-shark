import React, { Fragment } from 'react';
import { TextInput } from '../../../hooks';
import { createClassName } from '../../../utils';
import { MDBInput } from 'mdbreact';

type FormInputProps = {
	readonly textInput: TextInput;
	readonly isInputInvalid?: boolean;
	readonly typeInput: string;
	readonly labelInput: string;
	readonly disabled?: boolean;
};

const FormInput = React.memo<FormInputProps>(({ textInput, isInputInvalid, typeInput, labelInput, disabled }) => {
	const { hasValue, bindToInput } = textInput;

	//for validation soon
	//const className = createClassName(['input', 'is-medium', isInputInvalid && !hasValue && 'is-danger']);

	return (
		<Fragment>
			<div className="col-sm w-100 px-5">
				<MDBInput hint={textInput.value} {...bindToInput} type={typeInput} label={labelInput} disabled={disabled} />
			</div>
		</Fragment>
	);
});

FormInput.displayName = 'FormInput';

export default FormInput;
