import styled from 'styled-components';

export const StyledFormTitle = styled.div`
	text-transform: uppercase;
	font-weight: 700;
	padding-top: 10px;
	padding-bottom: 5px;
	font-family: Arial, Helvetica, sans-serif;
`;
