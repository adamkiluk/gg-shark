import React, { useState, useCallback, FormEvent, ReactNode, Fragment } from 'react';
import { connect } from 'react-redux';
import { IApplicationState } from '../../store';
import { actionCreators, reducer, ISignboard } from '../../store/JobForm';
import { MDBBtn, MDBInput, MDBBadge, MDBIcon } from 'mdbreact';
import { FormInput, SeniorityLevels } from './child-components';
import { useTextInput } from '../../hooks';
import { Checkbox } from '../../components';

import _ from 'lodash';
import './child-components/styles.css';

type JobFormProps = ReturnType<typeof reducer> & typeof actionCreators;

const JobForm: React.FC<JobFormProps> = ({ newSignboard, setSkill, unsetSkill, resetState, sendJobOffer }) => {
	const titleInput = useTextInput();
	const salaryFrom = useTextInput();
	const salaryTo = useTextInput();
	const mustHaveSkills = useTextInput();
	const nicetoHaveSkills = useTextInput();
	const dailyTasks = useTextInput();

	const [isInputInvalid, setIsInputInvalid] = useState<boolean>(false);

	const [hiddenRange, sethiddenRangeCheckbox] = useState<boolean>(newSignboard.hiddenRange);
	const onhiddenRangeCheck = useCallback((checked: boolean): void => sethiddenRangeCheckbox(checked), []);

	const [isSalaryRangeDisabled, setSalaryRangeDisabled] = useState<boolean>(false);

	const [about, setAboutInput] = useState<string>('');
	const onAboutChange = useCallback((e: FormEvent<HTMLInputElement>): void => setAboutInput(e.currentTarget.value), []);

	const handleBadgesButton = (type: string): void => {
		switch (type) {
			case 'mustHave':
				!_.includes(newSignboard.mustHave, mustHaveSkills.value) &&
					mustHaveSkills.value !== '' &&
					setSkill(type, mustHaveSkills.value);
				mustHaveSkills.clear();
				break;

			case 'niceToHave':
				!_.includes(newSignboard.niceToHave, nicetoHaveSkills.value) &&
					nicetoHaveSkills.value !== '' &&
					setSkill(type, nicetoHaveSkills.value);
				nicetoHaveSkills.clear();
				break;

			case 'dailyTasks':
				!_.includes(newSignboard.dailyTasks, dailyTasks.value) && dailyTasks.value !== '' && setSkill(type, dailyTasks.value);
				dailyTasks.clear();
				break;

			default:
				return;
		}
	};

	const removeBadge = (event: FormEvent, type: string): void => {
		let itemToRemove = (event.currentTarget as HTMLTextAreaElement).innerText;
		switch (type) {
			case 'mustHave':
				unsetSkill(type, _.difference(newSignboard.mustHave, [itemToRemove]));
				break;
			case 'niceToHave':
				unsetSkill(type, _.difference(newSignboard.niceToHave, [itemToRemove]));
				break;
			case 'dailyTasks':
				itemToRemove = (event.currentTarget.previousSibling as HTMLTextAreaElement).innerText;
				unsetSkill(type, _.difference(newSignboard.dailyTasks, [itemToRemove]));
				break;
			default:
				return;
		}
	};

	const handleBadges = (arr: Array<string>, type: string, color: string): ReactNode => {
		if (arr === undefined) return;
		return arr.map(
			(item: string): ReactNode => {
				return (
					<Fragment key={type + item}>
						<MDBBadge color={color} onClick={(event: FormEvent) => removeBadge(event, type)} className="m-1">
							{item}
							<MDBIcon className="p-1" icon="times-circle" size="lg" />
						</MDBBadge>
					</Fragment>
				);
			},
		);
	};

	const handleTasksBadges = (arr: Array<string>, type: string): ReactNode => {
		if (arr === undefined) return;
		return arr.map(
			(item: string): ReactNode => {
				return (
					<Fragment key={type + item}>
						<div className="d-flex justify-content-between">
							<div>
								<MDBIcon className="p-1" icon="angle-right" size="lg" />
								<span>{item}</span>
							</div>
							<MDBBadge color="danger" onClick={(event: FormEvent) => removeBadge(event, type)} className="m-1">
								Delete
							</MDBBadge>
						</div>
						<br></br>
					</Fragment>
				);
			},
		);
	};
	const handlehiddenRangeCheck = (e: boolean): void => {
		setSalaryRangeDisabled(e);
		onhiddenRangeCheck(e);
	};
	const handleResetState = (): void => {
		resetState();
		setAboutInput('');
		titleInput.clear();
		salaryFrom.clear();
		salaryTo.clear();
		mustHaveSkills.clear();
		nicetoHaveSkills.clear();
		dailyTasks.clear();
	};

	const handleSend = (): void => {
		const offer: ISignboard = {
			title: titleInput.value,
			seniorityLevel: newSignboard.seniorityLevel,
			about: about,
			hiddenRange: hiddenRange,
			salaryFrom: Number(salaryFrom.value),
			salaryTo: Number(salaryTo.value),
			mustHave: newSignboard.mustHave,
			niceToHave: newSignboard.niceToHave,
			dailyTasks: newSignboard.dailyTasks,
		};
		sendJobOffer(offer);
		//handleResetState();
	};

	return (
		<React.Fragment>
			<div className="mt-3 container border border-light">
				<h5 className="secondary-heading font-weight-bold pt-3 pl-5">Primary informations:</h5>
				<div className="row half-width px-4">
					<FormInput
						textInput={titleInput}
						typeInput="Text"
						labelInput="Title"
						isInputInvalid={isInputInvalid}
						disabled={false}
					/>
				</div>
				<SeniorityLevels />
				<div className="container px-4 mx-4"></div>
				<div className="mx-4 px-4">
					<MDBInput type="textarea" value={about} onChange={onAboutChange} label="Describe Your job offer here!" rows="4" />
				</div>
				<div className="d-flex justify-content-between">
					<FormInput
						textInput={salaryFrom}
						typeInput="Number"
						labelInput="From (PLN)"
						isInputInvalid={isInputInvalid}
						disabled={isSalaryRangeDisabled}
					/>
					<FormInput
						textInput={salaryTo}
						typeInput="Number"
						labelInput="To (PLN)"
						isInputInvalid={isInputInvalid}
						disabled={isSalaryRangeDisabled}
					/>
				</div>
				<div className="mx-4 px-4">
					<Checkbox label="Hide salary range for this offer." checked={hiddenRange} onCheck={(e) => handlehiddenRangeCheck(e)} />
				</div>
				<h5 className="secondary-heading font-weight-bold pt-3 pl-5">Skills:</h5>
				<div className="mx-4 px-4">{handleBadges(newSignboard?.mustHave, 'mustHave', 'info')}</div>
				<div className="d-flex justify-content-between">
					<FormInput
						textInput={mustHaveSkills}
						typeInput="Text"
						labelInput="Must have skills"
						isInputInvalid={isInputInvalid}
						disabled={false}
					/>
					<div className="pt-3 h-75 m-0 align-middle">
						<MDBBtn className="px-3 py-2" color="indigo" onClick={() => handleBadgesButton('mustHave')}>
							+
						</MDBBtn>
					</div>
				</div>
				<div className="mx-4 px-4">{handleBadges(newSignboard?.niceToHave, 'niceToHave', 'warning')}</div>
				<div className="d-flex justify-content-between">
					<FormInput
						textInput={nicetoHaveSkills}
						typeInput="Text"
						labelInput="Nice to have skills"
						isInputInvalid={isInputInvalid}
						disabled={false}
					/>
					<div className="pt-3 h-75 m-0 align-middle">
						<MDBBtn className="px-3 py-2" color="indigo" onClick={() => handleBadgesButton('niceToHave')} rounded>
							+
						</MDBBtn>
					</div>
				</div>
				<h5 className="secondary-heading font-weight-bold pt-3 pl-5">Daily tasks:</h5>
				<div className="mx-4 px-4">{handleTasksBadges(newSignboard?.dailyTasks, 'dailyTasks')}</div>
				<div className="d-flex justify-content-between">
					<FormInput
						textInput={dailyTasks}
						typeInput="Text"
						labelInput="Daily tasks"
						isInputInvalid={isInputInvalid}
						disabled={false}
					/>
					<div className="pt-3 h-75 m-0 align-middle">
						<MDBBtn className="px-3 py-2" color="indigo" onClick={() => handleBadgesButton('dailyTasks')}>
							+
						</MDBBtn>
					</div>
				</div>
			</div>
			<div className="mt-3 container d-flex justify-content-endd-flex justify-content-end">
				<MDBBtn onClick={handleResetState} color="danger">
					RESET
				</MDBBtn>
				<MDBBtn onClick={handleSend} color="indigo">
					SAVE
				</MDBBtn>
			</div>
		</React.Fragment>
	);
};

const mapStateToProps = (state: IApplicationState) => state.jobForm;

export default connect(mapStateToProps, actionCreators)(JobForm as any);
