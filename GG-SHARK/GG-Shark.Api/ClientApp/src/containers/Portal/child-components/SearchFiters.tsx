import React, { Fragment, ReactNode, FormEvent, useState } from 'react';
import { actionCreators, reducer } from '../../../store/Portal';
import { IApplicationState } from '../../../store';
import { connect } from 'react-redux';
import { technologyTypes, filterTypes } from '../../../helpers/constantsTypes';
import { MDBContainer, MDBBtn, MDBModal, MDBModalHeader, MDBModalBody, MDBModalFooter } from 'mdbreact';

type SearchFiltersProps = ReturnType<typeof reducer> & typeof actionCreators;

const SearchFilters: React.FC<SearchFiltersProps> = ({}) => {
	const [technologiesVisibility, setTechnologiesModalVisible] = useState<boolean>(false);
	const [locationsVisibility, setLocationsModalVisible] = useState<boolean>(false);
	const [salariesVisibility, setSalaryModalVisible] = useState<boolean>(false);

	const handleClickedFilter = (event: FormEvent): void => {
		let textContent = (event.currentTarget as HTMLTextAreaElement).innerText;
		switch (textContent) {
			case 'TECHNOLOGIES':
				setTechnologiesModalVisible(true);
				break;
			// case 'locations':
			// 	return handleLocationTypes1();
			// case 'salary':
			// 	return handleLocationTypes1();
			default:
				return;
		}
	};

	const renderTechnologiesModal = (): ReactNode => {
		return (
			<MDBContainer>
				<MDBModal
					isOpen={technologiesVisibility}
					toggle={() => setTechnologiesModalVisible(!technologiesVisibility)}
					backdrop={false}
				>
					<MDBModalHeader toggle={() => setTechnologiesModalVisible(!technologiesVisibility)}>MDBModal title</MDBModalHeader>
					<MDBModalBody>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
						aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					</MDBModalBody>
					<MDBModalFooter>
						<MDBBtn color="secondary" onClick={() => setTechnologiesModalVisible(!technologiesVisibility)}>
							Close
						</MDBBtn>
						<MDBBtn color="primary">Save changes</MDBBtn>
					</MDBModalFooter>
				</MDBModal>
			</MDBContainer>
		);
	};

	const handleLocationTypes1 = (): ReactNode => {
		return filterTypes.map(
			(item: string): ReactNode => {
				return (
					<MDBContainer className="w-auto m-0 p-0" key={item}>
						<MDBBtn outline color="info" size="sm" onClick={(event: FormEvent) => handleClickedFilter(event)}>
							{item}
						</MDBBtn>
					</MDBContainer>
				);
			},
		);
	};

	const handleLocationTypes = (): ReactNode => {
		return filterTypes.map(
			(item: string): ReactNode => {
				return (
					<MDBContainer className="w-auto m-0 p-0" key={item}>
						<MDBBtn outline color="info" size="sm" onClick={(event: FormEvent) => handleClickedFilter(event)}>
							{item}
						</MDBBtn>
					</MDBContainer>
				);
			},
		);
	};

	return (
		<Fragment>
			<div className="d-flex">{handleLocationTypes()}</div>
			{renderTechnologiesModal()}
		</Fragment>
	);
};

const mapStateToProps = (state: IApplicationState) => state.portal;

export default connect(mapStateToProps, actionCreators)(SearchFilters as any);
