import React, { Fragment } from 'react';
import { MDBInput, MDBBtn } from 'mdbreact';

const SearchBar = React.memo(({}) => {
	return (
		<Fragment>
			<div className="deep-blue-gradient position-relative">
				<div className="w-100 m-0 p-0">
					<div className="container">
						<div className="row justify-content-md-center">
							<div className="col col-lg-5">
								<MDBInput hint="SEARCH YOUR JOB!" type="text" containerClass="pt-5 mt-0 " color="white" />
							</div>
							<div className="col col-lg-2 d-flex align-items-end">
								<MDBBtn className="mb-4" color="warning">
									SEARCH
								</MDBBtn>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Fragment>
	);
});

SearchBar.displayName = 'SearchBar';

export default SearchBar;
