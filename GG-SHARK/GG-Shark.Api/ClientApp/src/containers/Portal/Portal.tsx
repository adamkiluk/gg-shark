import React, { Fragment } from 'react';
import { actionCreators, reducer } from '../../store/Portal';
import { IApplicationState } from '../../store';
import { connect } from 'react-redux';
import { SearchBar, SearchFiters } from './child-components';

type PortalProps = ReturnType<typeof reducer> & typeof actionCreators;

const Portal: React.FC<PortalProps> = ({}) => {
	return (
		<Fragment>
			<SearchBar />
			<div className="container">
				<SearchFiters />
			</div>
		</Fragment>
	);
};

const mapStateToProps = (state: IApplicationState) => state.portal;

export default connect(mapStateToProps, actionCreators)(Portal as any);
