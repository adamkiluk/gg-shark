import React, { Fragment, useEffect, useState } from 'react';
import { TextInput } from '../../../hooks';
import { createClassName } from '../../../utils';
import { MDBInput } from 'mdbreact';

type AccountInputProps = {
	readonly textInput: TextInput;
	readonly isInputInvalid?: boolean;
	readonly typeInput: string;
	readonly labelInput: string;
	readonly disabled?: boolean;
};

const AccountInput = React.memo<AccountInputProps>(({ textInput, isInputInvalid, typeInput, labelInput, disabled }) => {
	const { hasValue, bindToInput } = textInput;

	//const className = createClassName(['input', 'is-medium', isInputInvalid && !hasValue && 'is-danger']);
	return (
		<Fragment>
			<div className="col-sm w-100 px-5">
				<MDBInput hint={textInput.value} {...bindToInput} type={typeInput} label={labelInput} disabled={disabled} />
			</div>
		</Fragment>
	);
});

AccountInput.displayName = 'AccountInput';

export default AccountInput;
