import React, { useEffect, useState, useCallback } from 'react';
import { connect } from 'react-redux';
import { IApplicationState } from '../../store';
import { actionCreators, reducer, IAccount } from '../../store/Account';
import { MDBBtn } from 'mdbreact';
import { Checkbox } from '../../components';
import { AccountInput } from './child-components';
import { useTextInput } from '../../hooks';

type AccountProps = ReturnType<typeof reducer> & typeof actionCreators;

const Account: React.FC<AccountProps> = ({ getUser, editUser, userAccount, userCompany }) => {
	const emailInput = useTextInput(userAccount.email);
	const nameInput = useTextInput(userAccount.name);
	const phoneNumberInput = useTextInput(userAccount.phoneNumber);
	const repositoryInput = useTextInput(userAccount.repository);
	const websiteInput = useTextInput(userAccount.website);
	const facebookInput = useTextInput(userAccount.facebook);
	const linkedinInput = useTextInput(userAccount.linkedin);

	const [isInputInvalid, setIsInputInvalid] = useState<boolean>(false);

	const [terms, setTermsCheckbox] = useState<boolean>(userAccount.termsAndPolicy);
	const [newsletterSub, setNewsletterSub] = useState<boolean>(userAccount.newsletterSub);

	const onTermsCheck = useCallback((checked: boolean): void => setTermsCheckbox(checked), []);
	const onNewsletterCheck = useCallback((checked: boolean): void => setNewsletterSub(checked), []);

	useEffect(() => {
		getUser();
	}, []);

	useEffect(() => {
		setTermsCheckbox(userAccount.termsAndPolicy);
		setNewsletterSub(userAccount.newsletterSub);
	}, [userAccount]);

	const handleSubmitUserData = (): void => {
		const account: IAccount = {
			email: emailInput.value,
			name: nameInput.value,
			phoneNumber: phoneNumberInput.value,
			repository: repositoryInput.value,
			website: websiteInput.value,
			facebook: facebookInput.value,
			linkedin: linkedinInput.value,
			termsAndPolicy: terms,
			newsletterSub: newsletterSub,
		};
		editUser(account);
	};

	return (
		<React.Fragment>
			<div className="mt-3 container border border-light">
				<h5 className="secondary-heading font-weight-bold pt-3 pl-5">User info</h5>
				<div className="row">
					<AccountInput
						textInput={emailInput}
						typeInput="email"
						labelInput="Your email"
						isInputInvalid={isInputInvalid}
						disabled={true}
					/>
					<AccountInput textInput={phoneNumberInput} typeInput="text" labelInput="Your number" isInputInvalid={isInputInvalid} />
				</div>
				<div className="row">
					<AccountInput
						textInput={nameInput}
						typeInput="text"
						labelInput="Your name & surename"
						isInputInvalid={isInputInvalid}
					/>
				</div>
				<div className="py-2 pl-4">
					<Checkbox
						label="I have read and agree to the content of the Terms and Conditions and the Privacy Policy."
						checked={terms}
						onCheck={onTermsCheck}
					/>
				</div>
				<div className="py-2 pl-4">
					<Checkbox
						label="I agree to send marketing information to my e-mail address regarding products and services offered by system."
						checked={newsletterSub}
						onCheck={onNewsletterCheck}
					/>
				</div>
			</div>
			<div className="mt-3 container border border-light">
				<h5 className="secondary-heading font-weight-bold pt-3 pl-5">User data</h5>
				<div className="row">
					<AccountInput textInput={repositoryInput} typeInput="text" labelInput="Repo" isInputInvalid={isInputInvalid} />
					<AccountInput textInput={websiteInput} typeInput="text" labelInput="Website" isInputInvalid={isInputInvalid} />
				</div>
				<div className="row">
					<AccountInput textInput={facebookInput} typeInput="text" labelInput="Facebook" isInputInvalid={isInputInvalid} />
					<AccountInput textInput={linkedinInput} typeInput="text" labelInput="LinkedIn" isInputInvalid={isInputInvalid} />
				</div>
			</div>
			<div className="mt-3 container border border-light">
				<h5 className="secondary-heading font-weight-bold pt-3 pl-5">Your Company : {userCompany.name}</h5>
			</div>

			<div className="mt-3 container">
				<MDBBtn color="primary" onClick={handleSubmitUserData}>
					Save
				</MDBBtn>
			</div>
		</React.Fragment>
	);
};

const mapStateToProps = (state: IApplicationState) => state.acc;

export default connect(mapStateToProps, actionCreators)(Account as any);
