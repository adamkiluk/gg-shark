import { BaseService } from './base.service';
import {} from '../store/Portal';

class PortalService extends BaseService {
	private static _portalService: PortalService;

	private constructor(controllerName: string) {
		super(controllerName);
	}

	public static get Instance(): PortalService {
		return this._portalService || (this._portalService = new this('Portal'));
	}
}

export const PortalApi = PortalService.Instance;
