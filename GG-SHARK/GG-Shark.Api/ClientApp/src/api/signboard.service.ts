import { BaseService } from './base.service';
import { ISignboard } from '../store/JobForm';
import Cookies from 'js-cookie';

class SignboardService extends BaseService {
	private static _signboardService: SignboardService;

	private constructor(controllerName: string) {
		super(controllerName);
	}

	public static get Instance(): SignboardService {
		return this._signboardService || (this._signboardService = new this('Signboard'));
	}

	public async sendSignboard(signboard: ISignboard): Promise<any> {
		const token: string = Cookies.get('authToken') as string;
		this.$http.defaults.headers.common['Authorization'] = token;
		await this.$http.post('Create', signboard);
	}
}

export const SignboardApi = SignboardService.Instance;
