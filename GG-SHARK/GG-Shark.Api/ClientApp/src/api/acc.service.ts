import { BaseService } from './base.service';
import { IAccount, EditUserModel } from '../store/Account/types';
import Cookies from 'js-cookie';

class AccService extends BaseService {
	private static _accService: AccService;
	private constructor(controllerName: string) {
		super(controllerName);
	}

	public static get Instance(): AccService {
		return this._accService || (this._accService = new this('Account'));
	}

	public async getUserAsync(): Promise<IAccount> {
		const token: string = Cookies.get('authToken') as string;
		this.$http.defaults.headers.common['Authorization'] = token;
		const {data} = await this.$http.get<IAccount>('GetUser');
		return data;
	}

	public async editUserAsync(model: EditUserModel): Promise<any> {
		await this.$http.post('EditUser', model);
	}
}

export const AccApi = AccService.Instance;
