export const badgeColors: Array<string> = ['default', 'primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light'];

export const technologyTypes: Array<string> = ['JavaScript', 'Angular', 'React', '.NET', 'C#', 'Java', 'Python', 'C++', 'Node', 'Other'];

export const locationTypes: Array<string> = ['Remote', 'Warszawa', 'Gdańsk', 'Poznań', 'Kraków', 'Wrocław', 'Gliwice', 'Białystok'];

export const filterTypes: Array<string> = ['Technologies', 'Locations', 'Salary'];
